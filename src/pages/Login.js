import axios from "axios";
import React, { useState } from "react";
import { InputGroup, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import "../style/login.css";

export default function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();

  // menggunakan database
  // const login = async (e) => {
  //     e.preventDefault();
  //     axios.get(" http://localhost:8000/users").then(({data}) => {
  //         const admin = data.find(
  //             (x) => x.username === username && x.password === password
  //         );
  //         if (admin) {
  //             Swal.fire({
  //                 icon: "success",
  //                 title: "masuk sebagai Admin",
  //                 showConfirmButton: false,
  //                 timer: 2500
  //             })
  //             localStorage.setItem("username" , admin.username)
  //             localStorage.setItem("id" , admin.id)
  //             navigate("/")
  //             setTimeout(() => {
  //                 window.location.reload();
  //             }, 1000)
  //         } else {
  //             Swal.fire({
  //                 icon: "error",
  //                 title: "Username atau password tidak valid",
  //                 showConfirmButton: false,
  //                 timer: 2500
  //             })
  //         }
  //     })
  // }

  const login = async (e) => {
    e.preventDefault();

    try {
      const { data, status } = await axios.post(
        "http://localhost:5000/users/sign-in",
        {
          email: email,
          password: password,
        }
      );

      //jika respon status 200/ ok
      if (status === 200) {
        Swal.fire({
          icon: "success",
          title: "Login berhasil",
          showCancelButton: false,
          timer: 1500,
        });
        localStorage.setItem("userId", data.data.users.id);
        localStorage.setItem("token", data.data.token);
        localStorage.setItem("role", data.data.users.role);
        navigate("/");
      }
    } catch (error) {
      Swal.fire({
        icon: "error",
        title: "Username atau password tidak valid",
        showCancelButton: false,
        timer: 1500,
      });
      console.log(error);
    }
  };
  return (
    <div
      style={{ backgroundColor: "#F0DBDB", width: "500px" }}
      className="container border my-5 pt-3 pb-5 px-5 login "
    >
      <div style={{ fontFamily: "public-sans", fontSize: "17px" }}>
        <h1 className="mb-5">Form Login </h1>
        <Form onSubmit={login} method="POST">
          <div className="mb-3">
            <Form.Label>
              <strong>Email</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Masukkan Email Anda..."
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="mb-3">
            <Form.Label>
              <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Masukkan Password Anda..."
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </InputGroup>
          </div>
          <button variant="primary" type="submit" className="mx-1 buton btn">
            Login
          </button>
          <p>
            Jika belum punya akun silahkan{" "}
            <a style={{ textDecoration: "none" }} href="/register">
              Register
            </a>
          </p>
        </Form>
      </div>
    </div>
  );
}
