import axios from "axios";
import React, { useEffect, useState } from "react";
import { InputGroup, Form, Modal } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function EditProfilUser() {
  const [show, setShow] = useState(false);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [image, setImage] = useState("");
  const [telepon, setTelepon] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const navigate = useNavigate();
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    axios
      .get(`http://localhost:5000/users/${localStorage.getItem("userId")}`)
      .then((response) => {
        const page = response.data.data;
        setNama(page.Nama);
        setNama(page.Alamat);
        setNama(page.Image);
        setNama(page.Telepon);
        setNama(page.Email);
        setNama(page.Password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const submitActionHandler = async (e) => {
    e.preventDefault();

    Swal.fire({
      title: "want to edit?",
      text: "You clicked the button!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, edit it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            `http://localhost:5000/users/${localStorage.getItem("userId")}
            `,
            {
              nama: nama,
              alamat: alamat,
              image: image,
              telepon: telepon,
              email: email,
              password: password,
            }
          )
          .then(() => {
            Swal.fire("Successfully", "Your data has been successfully edited");
          })
          .catch((error) => {
            console.log(error);
          });
        navigate("/profilUser");
        window.location.reload();
      }
    });
  };

  return (
    <div className="edit mx-5">
      <div
        style={{ height: "530px", lineHeight: "10px" }}
        className="container my-5"
      >
        <Form onSubmit={submitActionHandler}>
          <div className="name mb-3">
            <Form.Label>
              <strong>Nama</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="nama"
                value={nama}
                onChange={(e) => setNama(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <div className="name mb-3">
            <Form.Label>
              <strong>Alamat</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="alamat"
                value={alamat}
                onChange={(e) => setAlamat(e.target.value)}
                required
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Gambar</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="file"
                placeholder="gambar"
                value={image}
                onChange={(e) => setImage(e.target.value)}
                required
              />
            </InputGroup>
          </div>

          <div className="mb-3">
            <Form.Label>
              <strong>No Telepon</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="number"
                placeholder="no telepon"
                value={telepon}
                onChange={(e) => setTelepon(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <div className="mb-3">
            <Form.Label>
              <strong>Email</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="email"
                placeholder="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
            </InputGroup>
          </div>
          <div className="mb-3">
            <Form.Label>
              <strong>Password</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="password"
                placeholder="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
            </InputGroup>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-2">
            <button type="submit" className="btn btn-primary" onClick={submitActionHandler}>
              Save
            </button>
          </div>
        </Form>
        {/* <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton style={{ backgroundColor: "#F0DBDB" }}>
            <Modal.Title>Add </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={submitActionHandler}>
              <div className="mb-3">
                <Form.Label>
                  <strong>Nama</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="nama..."
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Alamat</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="alamat..."
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Gambar</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    type="file"
                    value={image}
                    onChange={(e) => setImage(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Telepon</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="telepon..."
                    type="number"
                    value={telepon}
                    onChange={(e) => setTelepon(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Email</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="email..."
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Password</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="telepon..."
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <button className="mx-1 button-btl btn" onClick={handleClose}>
                Close
              </button>
              <button
                type="submit"
                className="mx-1 button-btl btn"
                onClick={handleClose}
              >
                save
              </button>
            </Form>
          </Modal.Body>
        </Modal> */}
      </div>
    </div>
  );
}

export default EditProfilUser;
