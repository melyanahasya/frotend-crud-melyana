import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { InputGroup, Form, Modal } from "react-bootstrap";
import axios from "axios";
import Swal from "sweetalert2";

export default function Navbar() {
  const [show, setShow] = useState(false);
  const [gambar, setGambar] = useState("");
  const [nama_makanan, setNama_Makanan] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");

  const navigate = useNavigate();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  const add = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", gambar);
    formData.append("nama_makanan", nama_makanan);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    try {
      //library open source yg digunakan untuk request data melalui http
      await axios.post(
        "http://localhost:5000/Produk",
        formData,

        // headers berikut berfungsi untk method yg diakses oleh admin
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
            "Content-Type": "multipart/form-data",
          },
        }
      );
      setShow(false);
      Swal.fire({
        icon: "success",
        title: "Success added data",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  const logout = () => {
    window.location.reload();
    localStorage.clear();
    navigate("/login");
  };

  return (
    <div
      style={{
        fontFamily: "public-sans",
        fontSize: "19px",
        backgroundColor: "#F9F2ED",
      }}
    >
      <nav
        className="navbar navbar-expand-lg "
        style={{ backgroundColor: "##F0DBDB" }}
      >
        <div className="container-fluid">
          <a style={{ fontSize: "33px" }} className="navbar-brand" href="/">
            <strong>
              <i>Caffe Online</i>
            </strong>
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <a className="nav-link active" aria-current="page" href="/">
                  Home
                </a>
              </li>

              {localStorage.getItem("role") === "ADMIN" ? (
                <>
                  <li
                    style={{ fontSize: "30px" }}
                    className="nav-item float right "
                  >
                    <button className="btn" onClick={handleShow}>
                      Add
                    </button>
                  </li>
                  <li className="nav-item">
                    <a className="nav-link active" href="/crud">
                      Table
                    </a>
                  </li>
                </>
              ) : (
                <></>
              )}
              <li style={{ margin: "left" }} className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  List
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="/makanan">
                      Food
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a className="dropdown-item" href="/minuman">
                      Cold drinks
                    </a>
                  </li>
                  <li>
                    <hr className="dropdown-divider" />
                  </li>
                  <li>
                    <a className="dropdown-item" href="/minumanHangat">
                      Warm drink
                    </a>
                  </li>
                </ul>
              </li>
              {localStorage.getItem("role") !== null ? (
                <>
                  <li>
                    <button className="nav-item float-right border-0 bg-transparent">
                      <a
                        style={{ color: "red", fontSize: "19px" }}
                        className="btn"
                        onClick={logout}
                      >
                        Logout
                      </a>
                    </button>
                  </li>

                  <li className="nav-item troli">
                    <a className="nav-link active" href="/cart">
                      cart
                    </a>
                  </li>
                  <a
                    style={{
                      textDecoration: "none",
                      color: "black",
                      marginTop: "9px",
                      fontSize: "20px",
                    }}
                    href="/profilUser"
                  >
                    Profil User
                  </a>
                </>
              ) : (
                <li className="nav-item float-right border-0 bg-transparent">
                  <a
                    style={{ color: "green", fontSize: "19px" }}
                    className="btn"
                    href="/login"
                  >
                    Login
                  </a>
                </li>
              )}
            </ul>
          </div>

          <ul className="list">{}</ul>
        </div>
      </nav>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton style={{ backgroundColor: "#F0DBDB" }}>
          <Modal.Title>Add </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={add}>
            <div className="mb-3">
              <Form.Label>
                <strong>Image</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  onChange={(e) => setGambar(e.target.files[0])}
                  type="file"
                  required
                />
              </InputGroup>
            </div>

            <div className="mb-3">
              <Form.Label>
                <strong>Nama Makanan</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan nama Makanan"
                  value={nama_makanan}
                  onChange={(e) => setNama_Makanan(e.target.value)}
                  required
                />
              </InputGroup>
            </div>

            <div className="mb-3">
              <Form.Label>
                <strong>Deskripsi</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan deskripsi"
                  value={deskripsi}
                  onChange={(e) => setDeskripsi(e.target.value)}
                  required
                />
              </InputGroup>
            </div>

            <div className="mb-3">
              <Form.Label>
                <strong>Harga</strong>
              </Form.Label>
              <InputGroup className="d-flex gap-3">
                <Form.Control
                  placeholder="Masukkan Harga"
                  type="number"
                  value={harga}
                  onChange={(e) => setHarga(e.target.value)}
                  required
                />
              </InputGroup>
            </div>

            <button className="mx-1 button-btl btn" onClick={handleClose}>
              Close
            </button>
            <button
              type="submit"
              className="mx-1 button-btl btn"
              onClick={handleClose}
            >
              save
            </button>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
