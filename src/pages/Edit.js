import axios from "axios";
import React, { useEffect, useState } from "react";
import { InputGroup, Form } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";

function Edit() {
  const param = useParams();
  const [gambar, setGambar] = useState("");
  const [nama_makanan, setNama_Makanan] = useState("");
  const [deskripsi, setDeskripsi] = useState("");
  const [harga, setHarga] = useState("");

  const navigate = useNavigate();

  // Yang menggunakan JSON
  // useEffect(() => {
  //   axios
  //     .get("http://localhost:8000/makanan/" + param.id)
  //     .then((response) => {
  //       const newFood = response.data;
  //       setGambar(newFood.gambar);
  //       setNama_Makanan(newFood.nama_makanan);
  //       setDeskripsi(newFood.deskripsi);
  //       setHarga(newFood.harga);
  //     })
  //     .catch((error) => {
  //       alert("terjadi kesalahan sir" + error);
  //     });
  // }, []);

  // const submitActionHandler = async (event) => {
  //   event.preventDefault();
  //   await Swal.fire({
  //     title: 'want to edit?',
  //     text: "You clicked the button!",
  //     icon: 'question',
  //     showCancelButton: true,
  //     confirmButtonColor: '#5F8D4E',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, edit it!'
  //   })
  //     .then((result) => {
  //       if (result.isConfirmed) {
  //         axios.put("http://localhost:8000/makanan/" + param.id, {
  //           gambar: gambar,
  //           nama_makanan: nama_makanan,
  //           deskripsi: deskripsi,
  //           harga: Number(harga),
  //         });
  //         Swal.fire("Edit?", "Your file has been edit.", "success");
  //         window.location.reload();
  //       }
  //     })
  //     .then(() => {
  //       navigate("/crud");
  //       window.location.reload();
  //     })
  //     .catch((error) => {
  //       alert("Terjadi kesalahan" + error);
  //     });
  // };

  useEffect(() => {
    axios
      .get("http://localhost:5000/Produk/" + param.id)
      .then((response) => {
        const newFood = response.data.data;
        setGambar(newFood.gambar);
        setNama_Makanan(newFood.nama_makanan);
        setDeskripsi(newFood.deskripsi);
        setHarga(newFood.harga);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const Put = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("file", gambar);
    formData.append("nama_makanan", nama_makanan);
    formData.append("deskripsi", deskripsi);
    formData.append("harga", harga);

    try {
      await axios.put("http://localhost:5000/Produk/" + param.id, formData);

      Swal.fire({
        icon: "success",
        title: "Your data has been successfully edited",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        navigate("/crud");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="edit mx-5">
      <div className="container my-5">
        <Form onSubmit={Put}>
          <div className="name mb-3">
            <Form.Label>
              <strong>Gambar</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                required
                type="file"
                onChange={(e) => setGambar(e.target.files[0])}
              />
            </InputGroup>
          </div>
          <div className="name mb-3">
            <Form.Label>
              <strong>Nama Makanan</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="nama makanan"
                value={nama_makanan}
                onChange={(e) => setNama_Makanan(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="place-of-birth mb-3">
            <Form.Label>
              <strong>Deskripsi</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                placeholder="Deskripsi"
                value={deskripsi}
                onChange={(e) => setDeskripsi(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="mb-3">
            <Form.Label>
              <strong>Harga</strong>
            </Form.Label>
            <InputGroup className="d-flex gap-3">
              <Form.Control
                type="number"
                placeholder="harga"
                value={harga}
                onChange={(e) => setHarga(e.target.value)}
              />
            </InputGroup>
          </div>

          <div className="d-flex justify-content-end align-items-center mt-2">
            <button className="buton btn" type="submit">
              Save
            </button>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default Edit;
