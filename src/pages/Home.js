import Navbar from "./Navbar";
import "../style/home.css";
import Card from "react-bootstrap/Card";
import ListGroup from "react-bootstrap/ListGroup";
import axios from "axios";
import React, { useEffect, useState } from "react";
import { numberWithCommas } from "../utils/Utils";
import Swal from "sweetalert2";
import AOS from "aos";
import Footers from "./Footers";

export default function Home() {
  const [food, setFood] = useState([]);
  const [totalPage, setTotalPage] = useState([]);
  const [makanan, setMakanan] = useState("");

  // post lewat db.json
  // const AddKeranjang = async (Food) => {
  //   await axios.post("http://localhost:5000/cart/", Food);
  // Swal.fire("Good job!", "You clicked the button!", "success")
  //   .then(() => {})
  //   .catch((error) => {
  //     alert("terjadi kesalahan" + error);
  //   });
  // };

  // getAll lewat Json
  // const getAll = async () => {
  //   await axios
  //     .get("http://localhost:8000/makanan")
  //     .then((res) => {
  //       setFood(res.data);
  //     })
  //     .catch((error) => {
  //       alert("terjadi kesalahan" + error);
  //     });
  // };

  const getAll = async (page) => {
    // library opensource yg digunakan untk reques data melalui http
    await axios
      .get(
        `http://localhost:5000/Produk/all?nama_makanan=${makanan}&page=${page}`
      )
      .then((res) => {
        setFood(
          res.data.data.content.map((food) => ({
            ...food,
            qty: 1,
          }))
        );
        const data = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          data.push(index);
        }
        setTotalPage(data);
      })
      .catch((error) => {
        alert("terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  AOS.init();

  const plusQuantityProduk = (idx) => {
    const listProduk = [...food];
    if (listProduk[idx].qty >= 45) return;
    listProduk[idx].qty++;
    setFood(listProduk);
  };
  const minusQuantityProduk = (idx) => {
    const listProduk = [...food];
    listProduk[idx].qty--;
    if (listProduk[idx].qty < 1) return;
    setFood(listProduk);
  };

  const Chekout = async (Food) => {
    console.log(Food);
    await axios.post("http://localhost:5000/cart", {
      produkId: Food.id, //food.id itu dari console
      qty: Food.qty,
      usersId: localStorage.getItem("userId"),
    });
    Swal.fire("Good job!", "Successfully added to cart", "success")
      .then(() => {})
      .catch((error) => {
        alert("terjadi kesalahan" + error);
      });
  };

  return (
    <div
      style={{ fontFamily: "public-sans", backgroundColor: "#F9F2ED" }}
      className="all"
    >
      <Navbar />

      {/* carousel */}
      <div
        id="carouselExampleFade"
        className="carousel slide carousel-fade"
        data-bs-ride="carousel"
        style={{ marginRight: "100px" }}
      >
        <div className="carousel-inner">
          <br />
          <div className="carousel-item active">
            <img
              style={{ widht: "60%", height: "490px" }}
              src="https://cdn-2.tstatic.net/travel/foto/bank/images/promo-starbucks-artinacup-is-back.jpg"
              className="d-block w-100 h-28 px-5 py-0 pb-5"
              alt="Brewed Coffe"
            />
          </div>
          <div className="carousel-item">
            <img
              style={{ widht: "60%", height: "490px" }}
              src="https://www.starbucks.co.id/storage/image/temporary/summernote_image_1648202656.jpg"
              className="d-block w-100 h-28 px-5 py-0 pb-5"
              alt="..."
            />
          </div>
          <div className="carousel-item">
            <img
              style={{ widht: "60%", height: "490px" }}
              src=" https://www.starbucks.co.id/image/banner-promo-card.jpg"
              className="d-block w-100 h-28 px-5 py-0 pb-5"
              alt="..."
            />
          </div>
        </div>
        <button
          className="carousel-control-prev"
          type="button"
          data-bs-target="#carouselExampleFade"
          data-bs-slide="prev"
        >
          <span
            className="carousel-control-prev-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Previous</span>
        </button>
        <button
          className="carousel-control-next"
          type="button"
          data-bs-target="#carouselExampleFade"
          data-bs-slide="next"
        >
          <span
            className="carousel-control-next-icon"
            aria-hidden="true"
          ></span>
          <span className="visually-hidden">Next</span>
        </button>
      </div>

      <form
        style={{ padding: "30px", marginLeft: "80px", marginRight: "80px" }}
        className="d-flex"
        role="search"
      >
        <input
          className="form-control me-2"
          type="search"
          placeholder="Search"
          aria-label="Search"
          value={makanan}
          onChange={(e) => setMakanan(e.target.value)}
        />
        <button
          className="btn btn-outline-success"
          type="button"
          onClick={() => getAll(0)}
        >
          Search
        </button>
      </form>

      <br />
      <div>
        <h3>The Best Choise</h3>
        <hr />

        <div className="kartu">
          <div
            style={{
              gap: "50%",
              padding: "5%",
              backgroundColor: "#F9F2ED",
              border: "none",
            }}
            className="card"
          >
            <div className="d-flex align-content-start flex-wrap row gy-5">
              {food.map((food, idx) => {
                return (
                  <div key={food.id} className="col-md-12 col-lg-4 col-sm-3" >
                    <Card
                      className="card-container"
                      data-aos="fade-up"
                      data-aos-anchor-placement="top-center"
                    >
                      <Card.Img
                        className="card-img"
                        variant="top"
                        src={food.gambar}
                        alt=""
                      />
                      <Card.Body>
                        <Card.Title>{food.nama_makanan}</Card.Title>
                        <Card.Text style={{ fontSize: "17px" }}>
                          {food.deskripsi}
                        </Card.Text>
                      </Card.Body>
                      <ListGroup className="list-group-flush">
                        <ListGroup.Item className="listGroup-item">
                          Start From Rp {numberWithCommas(food.harga)}
                        </ListGroup.Item>
                      </ListGroup>

                      <Card.Body>
                        {localStorage.getItem("role") !== null ? (
                          <>
                            <div
                              style={{
                                display: "flex",
                                marginLeft: "40px",
                                height: "32px",
                              }}
                            >
                              <button
                                type="button"
                                className={`input-group-text ${
                                  food.qty <= 1 ? "grey" : "green"
                                }`}
                                onClick={() => minusQuantityProduk(idx)}
                              >
                                -
                              </button>
                              <div
                                style={{ width: "50px" }}
                                className="form-control text-center"
                              >
                                {food.qty}
                              </div>
                              <button
                                type="button"
                                className={`input-group-text ${
                                  food.qty >= 45 ? "grey" : "green"
                                }`}
                                onClick={() => plusQuantityProduk(idx)}
                              >
                                +
                              </button>

                              <button
                                className="chekout-button"
                                onClick={() => Chekout(food)}
                                type="button"
                              >
                                <i className="fas fa-cart-plus"></i>
                              </button>
                            </div>
                          </>
                        ) : (
                          <></>
                        )}

                        {/* <Card.Link
                          target="_blank"
                          href={food.viewmore}
                        >
                        </Card.Link> */}
                      </Card.Body>
                    </Card>
                  </div>
                );
              })}
            </div>
          </div>
        </div>

        {food.length == 0 ? (
          <h4>menu tidak tersedia</h4>
        ) : (
          <div style={{ justifyContent: "center", display: "flex" }}>
            <nav aria-label="Page navigation example">
              <ul className="pagination">
                <li class="page-item">
                  <button
                    style={{ color: "black" }}
                    class="page-link"
                    aria-label="Previous"
                  >
                    <span aria-hidden="true">&laquo;</span>
                  </button>
                </li>
                {totalPage.map((e, i) => (
                  <li key={i} className="page-item">
                    <span
                      style={{ color: "black" }}
                      className="page-link pointer"
                      onClick={() => getAll(e)}
                    >
                      {e + 1}
                    </span>
                  </li>
                ))}
                <li className="page-item">
                  <button
                    style={{ color: "black" }}
                    className="page-link"
                    aria-label="Next"
                  >
                    <span aria-hidden="true">&raquo;</span>
                  </button>
                </li>
              </ul>
            </nav>
          </div>
        )}
      </div>
      <Footers />
    </div>
  );
}
