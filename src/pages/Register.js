import axios from "axios";
import React, { useState } from "react";
import { InputGroup, Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import "../style/register.css";
import Swal from "sweetalert2";

export default function Register() {
  // Integrasi Menggunakan Springboot
  const navigate = useNavigate();
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [image, setImage] = useState("");
  const [telepon, setTelepon] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // const [userRegister, setUserRegister] = useState({
  //   nama: "",
  //   alamat: "",
  //   telepon: "",
  //   image: "",
  //   email: "",
  //   password: "",
  //   role: "USER",
  // });

  // const handleOnChange = (e) => {
  //   setUserRegister((currUser) => {
  //     return { ...currUser, [e.target.id]: e.target.value };
  //   });
  // };

  const register = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("telepon", telepon);
    formData.append("file", image);
    formData.append("email", email);
    formData.append("password", password);
    formData.append("role", "USER");

    try {
      await axios.post("http://localhost:5000/users/sign-up", formData);

      Swal.fire({
        icon: "success",
        title: "Berhasil register",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        navigate("/login");
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  // dari database json
  // const [email, setEmail] = useState("");
  // const [password, setPassword] = useState("");

  // const navigate = useNavigate();

  // const register = async (e) => {
  //   e.preventDefault();
  //   await axios.post("http://localhost:8000/users", {
  //     email: email,
  //     password: password,
  //   });
  //   {
  //     Swal.fire("Succes!", "You clicked the button!", "success")
  //       .then(() => {
  //         navigate("/");
  //         window.location.reload();
  //       })
  //       .catch((error) => {
  //         alert("Terjadi Kesalahan " + error);
  //       });
  //   }
  // };
  return (
    <div
      className="container border my-5 pt-3 pb-5 px-5 forum"
      style={{
        backgroundColor: " #f0dbdb",
        width: "500px",
        lineHeight: "6px",
        height: "570px",
      }}
    >
      <h1 className="mb-5">Form Register</h1>
      <Form onSubmit={register}>
        <div className="mb-3">
          <Form.Label>
            <strong>Email</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              id="email"
              placeholder="Masukkan Email..."
              required
              type="email"
              // dari database.json
              // value={email}
              // onChange={(e) => setEmail(e.target.value)}

              // dari integrasi springboot
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              // value={userRegister.email}
            />
          </InputGroup>
        </div>

        <div className="mb-3">
          <Form.Label>
            <strong>Password</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              className="form-control"
              id="password"
              placeholder="Masukkan Password..."
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              // value={userRegister.password}
            />
          </InputGroup>
        </div>

        <div className="mb-3">
          <Form.Label>
            <strong>Nama</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              id="nama"
              placeholder="Masukkan Nama..."
              type="name"
              value={nama}
              onChange={(e) => setNama(e.target.value)}
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Alamat</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              id="alamat"
              placeholder="Masukkan Alamat..."
              type="alamat"
              value={alamat}
              onChange={(e) => setAlamat(e.target.value)}
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>No Telepon</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              id="telepon"
              placeholder="Masukkan No Telepon..."
              type="telepon"
              value={telepon}
              onChange={(e) => setTelepon(e.target.value)}
            />
          </InputGroup>
        </div>
        <div className="mb-3">
          <Form.Label>
            <strong>Gambar</strong>
          </Form.Label>
          <InputGroup className="d-flex gap-3">
            <Form.Control
              id="image"
              placeholder="Masukkan Gambar..."
              type="file"
              onChange={(e) => setImage(e.target.files[0])}
            />
          </InputGroup>
        </div>
        <button variant="primary" type="submit" className="mx-1 buton btn">
          Register
        </button>
      </Form>
    </div>
  );
}
