import React, { useEffect, useState } from "react";
import "../style/profilUser.css";
import axios from "axios";
import Swal from "sweetalert2";
import { InputGroup, Form, Modal } from "react-bootstrap";

export default function ProfilUser() {
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [show, setShow] = useState(false);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [image, setImage] = useState("");
  const [telepon, setTelepon] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [foods, setFoods] = useState({
    nama: "",
    alamat: "",
    image: null,
    telepon: "",
    email: "",
    password: "",
  });

  const getProfil = async () => {
    await axios
      .get(`http://localhost:5000/users/${localStorage.getItem("userId")}`)
      .then((res) => {
        setFoods(res.data.data);
      })
      .catch((error) => {
        alert("terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getProfil();
  }, []);

  useEffect(() => {
    axios
      .get(`http://localhost:5000/users/${localStorage.getItem("userId")}`)
      .then((response) => {
        const newFood = response.data.data;
        setNama(newFood.nama);
        setAlamat(newFood.alamat);
        setImage(newFood.image);
        setTelepon(newFood.telepon);
        setEmail(newFood.email);
        setPassword(newFood.password);
      })
      .catch((error) => {
        alert("Terjadi Kesalahan " + error);
      });
  }, []);

  const Put = async (e) => {
    e.preventDefault();
    e.persist();

    const formData = new FormData();
    formData.append("nama", nama);
    formData.append("alamat", alamat);
    formData.append("telepon", telepon);
    formData.append("file", image);
    formData.append("email", email);
    formData.append("password", password);

    try {
      await axios.put(
        `http://localhost:5000/users/${localStorage.getItem("userId")}`,
        formData
      );

      Swal.fire({
        icon: "success",
        title: "Your data has been successfully edited",
        showConfirmButton: false,
        timer: 1500,
      });
      setTimeout(() => {
        window.location.reload();
      }, 1500);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <div className="container">
      <div className="content">
        <div style={{ display: "flex", gap: "80%" }}>
          <h4 style={{ textAlign: "left" }}>Profil saya</h4>

          <button
            style={{ color: "black", border: "none" }}
            type="button"
            onClick={handleShow}
          >
            <i class="fas fa-user-edit"></i>
          </button>
        </div>
        <p style={{ textAlign: "left" }}>
          Kelola informasi profil anda untuk mengontrol, melindungi dan
          mengamankan akun
        </p>

        {/* <hr /> */}
      </div>
      <div className="form-image">
        {/* <div className="profil1"> */}
        <div>
          <p className="form" onSubmit={getProfil}>
            <p>Nama : {foods.nama}</p>
            <p>Alamat : {foods.alamat}</p>
            <p>Telepon : {foods.telepon}</p>
            <p>Email: {foods.email}</p>
            <p>Password : *****</p>
          </p>
        </div>
        <div className="garis_verikal"></div>
        <div className="gambar1">
          <img className="gambar" src={foods.image} alt="" />
        </div>
        {/* </div> */}

        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton style={{ backgroundColor: "#F0DBDB" }}>
            <Modal.Title>Add </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={Put}>
              <div className="mb-3">
                <Form.Label>
                  <strong>Nama</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="nama..."
                    value={nama}
                    onChange={(e) => setNama(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Alamat</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="alamat..."
                    value={alamat}
                    onChange={(e) => setAlamat(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Gambar</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    type="file"
                    onChange={(e) => setImage(e.target.files[0])}
                    required
                  />
                </InputGroup>
              </div>

              <div className="mb-3">
                <Form.Label>
                  <strong>Telepon</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="No telepon..."
                    type="number"
                    value={telepon}
                    onChange={(e) => setTelepon(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Email</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="email..."
                    type="text"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>
              <div className="mb-3">
                <Form.Label>
                  <strong>Password</strong>
                </Form.Label>
                <InputGroup className="d-flex gap-3">
                  <Form.Control
                    placeholder="password..."
                    type="password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </InputGroup>
              </div>

              <button className="mx-1 button-btl btn" onClick={handleClose}>
                Close
              </button>
              <button
                type="submit"
                className="mx-1 button-btl btn"
                onClick={handleClose}
              >
                save
              </button>
            </Form>
          </Modal.Body>
        </Modal>
      </div>
    </div>
  );
}
