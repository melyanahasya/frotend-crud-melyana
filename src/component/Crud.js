import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";
import { numberWithCommas } from "../utils/Utils";
import "../style/crud.css";

export default function Cart() {
  const [food, setFood] = useState([]);
  const [makanan, setMakanan] = useState([]);
  const [totalPage, setTotalPage] = useState([]);

  const getAll = async (page=0) => {
    await axios
      .get(`http://localhost:5000/Produk/all?nama_makanan=${makanan}&page=${page}`)
      .then((res) => {
        setFood(
          res.data.data.content.map((food) => ({
            ...food,
            qty: 1,
          }))
        );
        const data = [];
        for (let index = 0; index < res.data.data.totalPages; index++) {
          data.push(index);
        }
        setTotalPage(data);
      })
      .catch((error) => {
        alert("terjadi kesalahan " + error);
      });
  };

  useEffect(() => {
    getAll(0);
  }, []);

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Want to delete?",
      text: "You clicked the button!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios.delete(`http://localhost:5000/Produk/` + id);
      }
      window.location.reload();
    });
    getAll();
  };

  useEffect(() => {
    getAll();
  }, []);

  return (
    <div
      style={{ fontFamily: "public-sans", fontSize: "19px" }}
      className="all-container"
    >
      <div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Gambar</th>
              <th style={{ width: "200px" }} scope="col">
                Nama Produk
              </th>
              <th style={{ width: "500px" }} scope="col">
                Deskripsi
              </th>
              <th scope="col">Harga (Rp)</th>
              {localStorage.getItem("role") !== null ? <th>Action</th> : <></>}
            </tr>
          </thead>
          <tbody>
            {food.map((food, index) => {
              return (
                <tr key={food.id}>
                  <td>{index + 1}</td>
                  <td>
                    <img style={{ width: "80px" }} src={food.gambar} alt="" />
                  </td>
                  <td>{food.nama_makanan}</td>
                  <td style={{ fontSize: "17px" }}>{food.deskripsi}</td>
                  <td>{numberWithCommas(food.harga)}</td>
                  {localStorage.getItem("role") !== null ? (
                    <td className="action">
                      <a href={"/edit/" + food.id}>
                        <button
                          variant="warning"
                          style={{ border: "none" }}
                          className="mx-1"
                        >
                          <i className="far fa-edit"></i>{" "}
                        </button>
                      </a>
                      ||
                      <button
                        variant="danger"
                        style={{ border: "none" }}
                        className="mx-1"
                        onClick={() => deleteUser(food.id)}
                      >
                        <i className="fas fa-trash-alt"></i>
                      </button>
                    </td>
                  ) : (
                    <></>
                  )}
                </tr>
              );
            })}
          </tbody>
        </table>

        <div style={{ justifyContent: "center", display: "flex" }}>
          <nav aria-label="Page navigation example">
            <ul className="pagination">
              <li class="page-item">
                <button
                  style={{ color: "black" }}
                  class="page-link"
                  aria-label="Previous"
                >
                  <span aria-hidden="true">&laquo;</span>
                </button>
              </li>
              {totalPage.map((e, i) => (
                <li key={i} className="page-item">
                  <span
                    style={{ color: "black" }}
                    className="page-link pointer"
                    onClick={() => getAll(e)}
                  >
                    {e + 1}
                  </span>
                </li>
              ))}
              <li className="page-item">
                <button
                  style={{ color: "black" }}
                  className="page-link"
                  aria-label="Next"
                >
                  <span aria-hidden="true">&raquo;</span>
                </button>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  );
}
