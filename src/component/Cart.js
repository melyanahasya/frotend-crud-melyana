import axios from "axios";
import React, { useEffect, useState } from "react";
import Swal from "sweetalert2";

export default function Cart() {
  const [food, setFood] = useState([]);
  const [totalHarga, setTotalHarga] = useState(0);
  const [totalPage, setTotalPage] = useState([]);
  // const total = food.reduce((a, b) => a + b.harga, 0); // buat total harga

  // dari db.json
  // const AddPesanan = async (food) => {
  //   await axios.post(" http://localhost:8000/pesanan", food);
  //   Swal.fire("Good job!", "You clicked the button!", "question")
  //     .then(() => {})
  //     .catch((error) => {
  //       alert("terjadi kesalahan" + error);
  //     });
  // };
  
  // integrasi chekout dari backend
  const ChekoutPesanan = async () => {
    await axios.delete(`http://localhost:5000/cart/deleteAll?users_id=${localStorage.getItem("userId")}`);
    await Swal.fire("Good job!", "Successfully checked out", "success")
    getAll();
    };

  // const deleteUser = async (id) => {
  //   Swal.fire({
  //     title: 'Are you sure?',
  //     text: "You clicked the button!",
  //     icon: 'question',
  //     showCancelButton: true,
  //     confirmButtonColor: '#3085d6',
  //     cancelButtonColor: '#d33',
  //     confirmButtonText: 'Yes, delete it!'
  //   }).then((result) => {
  //     if (result.isConfirmed) {
  //       Swal.fire(
  //         'Deleted!',
  //         'Your file has been deleted.',
  //         'success'
  //       )
  //     }
  //   }).then((result) => {
  //       axios.delete("http://localhost:8000/keranjang/" + id);
  //       window.location.reload();
  //   });
  //   getAll();
  // };

  const deleteUser = async (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You clicked the button!",
      icon: "question",
      showCancelButton: true,
      confirmButtonColor: "#5F8D4E",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!",
    }).then(async (result) => {
      if (result.isConfirmed) {
        await axios.delete("http://localhost:5000/cart/" + id);
        getAll();
      }
    });
  };

  const getAll = async (page = 0) => {
    const res = await axios.get(
      `http://localhost:5000/cart?page=${page}&users_id=${localStorage.getItem("userId")}`
    );
    setFood(
      res.data.content.map((food) => ({
        ...food,
        checked: false,
      }))
    );
    const data = [];
        for (let index = 0; index < res.data.totalPages; index++) {
          data.push(index);
        }
    let total = 0;
    res.data.content.forEach((food) => {
      total += food.totalHarga; // rumus total harga
    });
    setTotalHarga(total);
    setTotalPage(data);
    // console.log(res.data.content);
  };


  const konversionRupiah = (angka) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0, //untuk menghilangkan angka 0 dibelakang koma
    }).format(angka);
  };

  useEffect(() => {
    getAll();
  }, []);
  return (
    <div style={{ fontFamily: "public-sans", fontSize: "19px" }}>
      <div>
        <table className="table">
          <thead>
            <tr>
              <th scope="col">No</th>
              <th scope="col">Image</th>
              <th style={{ width: "200px" }} scope="col">
                Produck
              </th>
              <th style={{ width: "500px" }} scope="col">
                Description
              </th>
              <th scope="col">Price (Rp)</th>
              {localStorage.getItem("userId") !== null ? (
                <th>Action</th>
              ) : (
                <></>
              )}
            </tr>
          </thead>
          <tbody>
            {food.map((food, index) => (
              <tr key={food.id}>
                <td>{index + 1}</td>
                <td>
                  <img
                    style={{ width: "80px" }}
                    src={food.produk.gambar}
                    alt=""
                  />
                </td>
                <td>{food.produk.nama_makanan}</td>
                <td style={{ fontSize: "17px" }}>{food.produk.deskripsi}</td>
                <td>{konversionRupiah(food.totalHarga)}</td>
                {localStorage.getItem("ADMIN") === null && (
                  <td className="action">
                    <button
                      variant="danger"
                      style={{ border: "none" }}
                      className="mx-1"
                      onClick={() => deleteUser(food.idCarts)}
                    >
                      <i className="fas fa-trash-alt"></i>
                    </button>
                  </td>
                )}
              </tr>
            ))}
          </tbody>
        </table>
        <strong
          style={{
            backgroundColor: "#F0DBDB",
            display: "block",
            width: "170px",
            padding: "5px",
            margin: "10px",
          }}
        >
          Total : {konversionRupiah(totalHarga)}
        </strong>
        <button
          style={{
            border: "none",
            display: "block",
            width: "170px",
            padding: "5px",
            margin: "10px",
            backgroundColor: "#F0DBDB",
          }}
          onClick={() => ChekoutPesanan(food)}
        >
          Chekout
        </button>
        
      </div>
      <div style={{ justifyContent: "center", display: "flex" }}>
            <nav aria-label="Page navigation example">
              <ul className="pagination">
                <li class="page-item">
                  <button
                    style={{ color: "black" }}
                    class="page-link"
                    aria-label="Previous"
                  >
                    <span aria-hidden="true">&laquo;</span>
                  </button>
                </li>
                {totalPage.map((page, index) => ( 
                  <li key={index} className="page-item">
                    <span
                      style={{ color: "black" }}
                      className="page-link pointer"
                      onClick={() => getAll(page)}
                    >
                      {page + 1}
                    </span>
                  </li>
                  ))}
                <li className="page-item">
                  <button
                    style={{ color: "black" }}
                    className="page-link"
                    aria-label="Next"
                  >
                    <span aria-hidden="true">&raquo;</span>
                  </button>
                </li>
              </ul>
            </nav>
          </div>
    </div>
  );
}
